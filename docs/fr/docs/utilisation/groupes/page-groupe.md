# Page d'un groupe

!!! note
    Sur la version 1.0.0 de Mobilizon, il n’est pas encore possible de demander à rejoindre un groupe : le bouton **rejoindre un groupe** est grisé sur les pages publiques de chaque groupe.

    Cependant, si nous avons affiché ce bouton (ainsi qu’un message expliquant que « Vous pouvez uniquement être invité aux groupes pour le moment »), c’est parce que nous voulons intégrer une telle fonctionnalité.

    Le fait est que, dans un outil fédéré, coder ce bouton soulève une multitude de cas complexes. Par conséquent, nous avons encore besoin de temps pour faire les choses correctement.

    En attendant, n’hésitez pas à commenter l’événement d’un groupe pour leur signaler que vous désirez y être invité·e !

## Signaler un groupe

Pour signaler un groupe, vous devez&nbsp;:

  1. cliquer sur le bouton **⋅⋅⋅**
  * cliquer sur le bouton **Signalement**

    ![bouton de signalement](../../images/report-group-FR.png)

  * [Optionnel mais **recommandé**] renseigner la raison du signalement&nbsp;:
    ![modale de signalement](../../images/report-group-modal-FR.png)
