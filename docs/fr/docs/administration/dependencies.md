# Dépendances
L'application Mobilizon a été conçue à partir de l'outil de réseautage social fédéré `Pleroma`.
L'application Mobilizon est donc conçue comme un serveur Web local qui repose sur les composants suivants :
 * une machine virtuelle Erlang,
 * le langage de programmation Elixir (version > 1.8) reposant sur la machine virtuelle,
* l'environnement de développement Web Phoenix reposant sur Elixir.
Il propose une application de type MVC (modèle-vue-contrôleur),  
* le gestionnaire de base de données PostgreSQL (version > 11) pour stocker les données manipulées par Phoenix,
* l'extension postgis pour gérer les données géographiques dans PostgreSQL,
* l'environnement d'exécution Node.js (version > 12) pour traiter les requêtes HTTP manipulées par Phoenix. [À VÉRIFIER]
De plus, les développeurs conseillent pour ouvrir le serveur Mobilizon sur le Web public d'utiliser un serveur Web NGINX comme passerelle (mandataire inversé).

## Debian & distributions filles 
L'installation de Mobilizon est présentée manuellement en plusieurs étapes.
La procédure d'installation a été testée sur Debian/TESTING-SID et devrait convenir pour des distributions comme Ubuntu 18.04 LTS.

### Rappel de sécurité

Comme tout serveur offrant ses services sur Internet, vérifiez la sécurité du système d'exploitation:
* si un serveur ssh actif, l'authentication par mot de passe doit être désactivée au profit d'une authentication par clef.
* installer __`Fail2Ban`__ pour éviter les tentatives répétées de connections illégales
* installer un pare-feu et bloquer les ports non utilisés

###  Mise à jour du système d'exploitation

Pour s'assurer d'avoir le système le plus à jour, appliquer :
```bash
sudo apt update
sudo apt -y dist-upgrade
```
Notes :
 L'outil apt des distributions basées sur Debian permettent de gérer les paquets de la distribution.
 L'option -y permet d'éviter d'être questionné par l'outil.

### Outils d'installation
Les outils suivants sont utilisés :
* `build-essential` outils de base pour compiler
* `curl` permet de télécharger des documents par HTTP(S) en ligne de commande
* `unzip` outil de gestion des chiers compressés ZIP 
* `vim` outil d'édition 
* `openssl` bibliothèques et outils SSL (pour HTTPS)
* `git` outil de manipulation des dépôts GIT 
* `cmake` outil d'assistance à la compilation
* `file` 

On commence donc par installer ces outils : 

```bash
sudo apt -y install build-essential curl unzip vim openssl git cmake file
```

### Service Web & Let's Encrypt
Le service Mobilizon repose sur le serveur Web nginx et le gestionnaire de certicats Let's Encrypt certbot

```bash
sudo apt -y install nginx certbot
```
 
On peut utiliser `certbot` comme un plugin ou manuellement. Voir la [documentation officielle](https://certbot.eff.org/instructions).


### NodeJS  

On installe la dernière verson de NodeJS en ajoutant le dépôt NodeSource. Voir la procédure détaillée sur la [documentation officielle](https://github.com/nodesource/distributions/blob/master/README.md#table-of-contents) en suivant les instruction pour `Node.js v12.x`. 

```bash
sudo su
curl -sL https://deb.nodesource.com/setup_12.x | bash -
apt-get install -y nodejs
exit
``` 

!!! info
   Sauf indication contraire, Mobilizon repose uniquement sur les versions LTS de NodeJS.
    
!!! tip
  NodeSource fournit des dépôts pour les mises à jour d'une version spécifique de NodeJS (ici la version 12. Pour les futures versions majeures il faudra adapter la commande en changeant le dépôt. 

### Yarn     
Mobilizon utilise  [Yarn](https://yarnpkg.com/) pour gérer les paquets NodeJS. 
Il faut donc l'installer comme ci-dessous, voir les instructions détaillées [sur cette page](https://yarnpkg.com/en/docs/install#debian-stable) 

```bash
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt install yarn
```

### Erlang et Elixir
Il semble que les paquets Debian par défaut ne soient pas assez à jour. Une installation à partir de paquets fournis par l'éditeur du logiciel est nécessaire.

Ajouter la ligne `deb https://packages.erlang-solutions.com/debian testing contrib` au fichier `/etc/apt/sources.list` puis le fichier clef : 
```bash
wget https://packages.erlang-solutions.com/debian/erlang_solutions.asc
sudo apt-key add erlang_solutions.asc
```
Une fois ceci fait, on met à jour les paquets disponibles : 
```bash
sudo apt update
sudo apt install erlang
sudo apt install elixir
```
 
Le paquet __`elixir`__ ajoute, entre autre, les outils 
* `Mix` un outil d'automatisation permettant de créer, gérer les dépendances, compiler et tester des projets Elixir.
* `Ecto`, un outil permettant à Elixir d'interagir avec des bases de données.
  

### Gestionnaire de base de données
Mobilizon utilise le gestionnaire de base de données PostgreSQL. Celui-ci doit être étendu pour gérer les données géographiques avec PostGIS. 
```bash
sudo apt -y install postgresql postgresql-contrib
sudo apt -y install --install-recommends postgresql-13-postgis-3
sudo systemctl enable postgresql # Activer le serveur PostgreSQL au démarrage
sudo systemctl start postgresql # lancer le serveur 
``` 
!!! Note: 
    `systemctl` est un outil permettant de gérer les services du sytème d'exploitation.   


### Divers
Pour gérer les images Mobilizon utilise ImageMagick.

```bash
sudo apt install imagemagick
```
Les outils d'optimisation suivants sont optionnels, Mobilizon peut fonctionner sans eux.  

```bash
sudo apt install webp gifsicle jpegoptim optipng pngquant
```

Une fois les outils correctement installés, vous pouvez [retourner à l'installation](index.md).
 
