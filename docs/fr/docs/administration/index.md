# Procédure d'installation pas à pas

!info sur Docker : [L'installation sur Docker (en anglais)](docker.md) est actuellement en phase Beta.

## Pré-requis
L'installation la plus simple demande :
* une machine Linux avec un accès superutilisateur (root)
* un nom de domaine pour cette machine, par ex. : mobilizon.votre-domaine
* un serveur SMTP pour pouvoir envoyer des mél

## Dépendances

L'application _Mobilizon_ repose sur un serveur Web local fournissant une application de type Modèle-Vue-Contrôleur. Celle-ci repose sur les composants suivants :
* une machine virtuelle Erlang,
* le langage de programmation Elixir (version > 1.8) reposant sur la machine virtuelle,
* l'environnement de développement Web de type Modèle-Vue-Contrôleur Phoenix reposant sur Elixir,
* le gestionnaire de base de données PostgreSQL (version > 11) pour stocker les données manipulées par Phoenix,
* l'extension postgis pour gérer les données géographiques dans PostgreSQL,
* l'environnement d'exécution Node.js (version > 12) _pour traiter les requêtes HTTP manipulées par Phoenix._ [À VÉRIFIER]
De plus, les développeurs conseillent pour ouvrir le serveur Mobilizon sur le Web public d'utiliser un serveur Web NGINX comme passerelle (mandataire inversé). 

Installez de préférence Elixir et NodeJS depuis leur dépôts officiels plutôt que ceux proposés par votre distribution. 
Versions recommandées :

* Elixir 1.8+
* NodeJS 12+
* PostgreSQL 11+

!!! important
  La façon d'installer les dépendances varie selon la distribution de votre système. Voir le [guide dédié](dependencies.md) et revenez ici une fois cette étape passée.

## Installation de Mobilizon

### Utilisateur
 Celle-ci sera réalisée par un utilisateur dédié nommé `mobilizon`.
 Création de l'utilisateur : 
```bash
sudo adduser --disabled-login mobilizon
```

 Note: On supposera que le répertoire personnel de l'utilisateur est `/home/mobilizon`  .


!!! note

    Avec FreeBSD
    
    ```bash
    sudo pw useradd -n mobilizon -d /home/mobilizon -s /usr/local/bin/bash -m 
    sudo passwd mobilizon
    ```

 Ouvrir une session shell avec cet utilisateur
```bash 
su mobilizon
``` 
### Cloner le dépôt git de Mobilizon
```bash 
git clone https://framagit.org/framasoft/mobilizon live
cd live
```

### Installer les dépendances d'Elixir : 

```bash 
mix deps.get
```
À la question, presser Enter

### Compiler les dépendances et Mobilizon
```bash
MIX_ENV=prod mix compile
```
### Installer les dépendances JavaScript
```bash
cd js
yarn install
```
Une fois les dépendances installées on peut construire l'interface (cela peut prendre quelques minutes). 

!!! __attention__
    Cette opération peut consommer plus de 2048Mo de RAM par défaut. 
    Pour l'adapter à votre configuration, vous pouvez ajuster le maximum de mémoire allouée en préfixant la commande avec la variable d'environnement: `NODE_BUILD_MEMORY=1024`

```bash
yarn build
cd ..
```

### Génération de la conguration de la base SQL

!!! note

    Pour cette partie il faudra préfixer toutes les commandes `mix`  avec la variable d'environnement `MIX_ENV=prod`.  

```bash
MIX_ENV=prod mix mobilizon.instance gen
```
Il vous sera demandé de préciser certains aspects de votre configuration et de votre instance pour générer un fichier `prod.secret.exs` dans le répertoire `config/` , et un fichier `setup_db.psql` pour configurer la base de données.

### Application de la configuration de la base SQL

Le fichier `setup_db.psql` contient les commandes SQL pour créer l'utilisateur et la base PostgreSQLavec les autorisations choisies à l'étape précédente et ajoute les extensions requises à la base Mobilizon.

Déconnectez l'utilisateur `mobilizon` (car il ne devrait pas avoir les droits de super admin `root`/`sudo` sur votre serveur) et exécutez la commande suivante depuis le dossier `/home/mobilizon/live` : 

```bash
exit 
su sudo -u postgres psql -f setup_db.psql
```

Cela devrait vous retourner quelques chose de ce genre:
```
CREATE ROLE
CREATE DATABASE
You are now connected to database "mobilizon_prod" as user "postgres".
CREATE EXTENSION
CREATE EXTENSION
CREATE EXTENSION
```

Reprenez votre utilisateur `mobilizon`:
```bash
sudo -i -u mobilizon
cd live
```

!!! __attention__

   Pensez à supprimer le fichier `setup_db.psql` quand c'est fini.

### Peuplement de la base
```bash
MIX_ENV=prod mix ecto.migrate
```

!!! note
    Si le peuplement / remplissage de la base échoue, cela signifie probablement que vous n'utilisez pas une version assez récente de PostgreSQL, ou que vous n'avez pas installé les extensions requises.  

## Service et serveur
### Service (Systemd)

On peut déconnecter l'utilisateur `mobilizon` une fois de plus.

```bash
sudo cp support/systemd/mobilizon.service /etc/systemd/system/ # copie du fichier de config
sudo systemctl enable mobilizon.service # activation du service au démarrage
sudo systemctl start mobilizon.service # lancement du service
sudo systemctl daemon-reload # prise en compte du fichier de configuration
```
On pourra vérier l'état du service par :
```bash
sudo journalctl -fu mobilizon.service
```

Vous devriez voir ce type de message apparaître :
```
Running Mobilizon.Web.Endpoint with cowboy 2.8.0 at :::4000 (http)
Access Mobilizon.Web.Endpoint at https://your-mobilizon-domain.com
```

### Interconnexion au serveur Web  (reverse proxy)
Le serveur Mobilizon écoute sur le portTCP/4000 de l'interface locale par défaut. Il est donc nécessaire d'utiliser un __mandataire inversé__ pour y accéder.

```bash
sudo cp support/nginx/mobilizon.conf /etc/nginx/sites-available # copie le fichier de config nginx
sudo ln -s /etc/nginx/sites-available/mobilizon.conf /etc/nginx/sites-enabled/ # lien symbolique pour activation du fichier 
``` 

Adaptez le fichier `/etc/nginx/sites-available/mobilizon.conf` à votre configuration avant de : 

```bash
sudo nginx -t # tester votre configuration du serveur 
sudo systemctl enable nginx # activation du service au démarrage 
sudo systemctl reload nginx # lancement du service
```

### Génération du certificat Let's Encrypt

La configuration de nginx utilise la méthode HTTP-01, le plugin `webroot`. Le programme `certbot` génère et gère les certificats de ses services :  
```bash
sudo mkdir /var/www/certbot
```

Lancez certbot (sans oublier d'adapter les arguments de la commande) :
```bash
sudo certbot certonly --rsa-key-size 4096 --webroot -w /var/www/certbot/ --email your@email.com --agree-tos --text --renew-hook "/usr/sbin/nginx -s reload" -d mobilizon.votre-domaine
```

Adaptez la configuration nginx `/etc/nginx/sites-available/mobilizon.conf` en décommentant le chemin du certificat et en supprimant les blocs obsolètes.  

Pour finir, testez la configuration avec `sudo nginx -t` et en relançant `sudo systemctl reload nginx`.

Vous devriez pouvoir maintenant accèder à https://mobilizon.votre-domaine.com depuis votre navigateur.

### Précautions à prendre 

Comme tout serveur offrant ses services sur Internet, vérifiez la sécurité du système d'exploitation:
* si un serveur ssh actif, l'authentication par mot de passe doit être désactivée au profit d'une authentication par clef.
* installer __`Fail2Ban`__ pour éviter les tentatives répétées de connections illégales
* installer un pare-feu et bloquer les ports non utilisés

## Création de votre premier utilisateur

Reprenez la main avec l'utilisateur `mobilizon` sur votre système:

```bash
sudo -i -u mobilizon
cd live
```

Créez le premier utilisateur, __l'administrateur__ de votre instance :
```
 MIX_ENV=prod mix mobilizon.users.new "your@email.com" --admin --password "Y0urP4ssw0rd"
```

!!! danger
    N'oubliez pas de préfixer la commande avec un espace vide de façon à ce que votre mot de passe ne soit pas enregistré dans votre historique shell. 

!!! astuce
    Si vous omettez l'option `--password`, Mobilizon en générera un pour vous.

Voir la [documentation complète](../../Administration/CLI tasks/manage_users.md#create-a-new-user) de cette commande.

Vous pouvez maintenant vous connecter sur l'interface et découvrir Mobilizon. Appuyez-vous sur [la doc de configuration](../../Administration/configure) pour aller plus loin.

## Étapes suivantes (suggestions) 

### Configurer les méls

Par défaut Mobilizon suppose qu'un serveur SMTP local est disponible sur le même serveur. Pour utiliser un serveur externe, rendez-vous [sur cette page](../../Administration/configure/email.md).

### Configurer authentification par tiers 
Mobilizon peut utiliser des tiers d'identification LDAP ou OAuth pour permettre l'enregistrement et la connection de nouveaux utilisateurs à votre instance grâce à leur compte externe (F***b***, G***l*, etc...). La configuration de cette option [est par ici](../../Administration/configure/auth.md).

_cette option permet-elle en fait l'auhentification depuis d'autres services fédérés comme Mastodon ?_

### Configurer le géocodage

Cette option permet l'auto-complétion du champs adresse lorsqu'on crée un événement. Par défaut, le service utilise le service Nominatim d'OpenStreetMap comme fournisseur de cartes mais vous pouvez [choisir le fournisseur que vous souhaitez](../../Administration/configure/geocoders.md).

!!! note
    En utilisant le service Nominatim d'OpenStreetMap, l'auto-complétion n'est pas accessible par défaut et vous devrez accepter leur [CGU](https://operations.osmfoundation.org/policies/nominatim/) pour en bénéficier.

### Bases de données de géolocalisation

Mobilizon peut utiliser la géolocalisation depuis des sources de données au format MMDB telles que  [MaxMind GeoIP](https://dev.maxmind.com/geoip/geoip2/geolite2/) ou [db-ip.com](https://db-ip.com/db/download/ip-to-city-lite). Cela permet au site de montrer les événements ayant lieu dans le voisinage de l'utilisateur.  

Vous devrez télécharger la base de donnés `City` dans `priv/data/GeoLite2-City.mmdb`, et redémarrer le service `mobilizon` après. 

Si cette base est manquante, Mobilizon n'affichera une alerte qu'une fois au démarrage mais elle n'est pas requise pour le fonctionnement.  

## Mise à jour
__à vérifier__
Se rendre dans le répertoire `/home/mobilizon/live`

```bash
cd /home/mobilizon/live 
git config pull.rebase false # configurer git
git pull # Mise à jour du dépôt local
mix deps.get # Mise à jour des dépendances
MIX_ENV=prod mix compile # peut prendre du temps et générer des alertes
MIX_ENV=prod mix mobilizon.instance gen # Régénération de l'instance
MIX_ENV=prod mix ecto.migrate # Migration de la base de donnée
```
