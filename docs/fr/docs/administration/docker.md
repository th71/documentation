# Docker

The docker-compose provides an app container for Mobilizon itself and a database container for PostgreSQL (Postgis). You'll need your own reverse-proxy to handle TLS termination (on port 4000).

## Update the env file

```bash
cd docker/production/
cp env.template .env
```

Edit the `.env` content with your own settings.
More settings can be added in .env, see all in docker-compose.yml file.

You can generate values for `MOBILIZON_INSTANCE_SECRET_KEY_BASE` and `MOBILIZON_INSTANCE_SECRET_KEY` with:

```bash
gpg --gen-random --armor 1 50
```

## Run the service

Start by initializing and running the database:

```bash
docker-compose up -d db
```

Instanciate required Postgres extensions:

```bash
docker-compose exec db psql -U <username>
```

```sql
CREATE EXTENSION pg_trgm;
CREATE EXTENSION unaccent;
```

Finally, run the application:

```bash
docker-compose up -d mobilizon
```

A migration will be automatically run before starting Mobilizon (can be run even if no migration is needed without incidence).

## Run a mobilizon_ctl command

```bash
docker-compose exec mobilizon mobilizon_ctl [options]
```

## Update the service

Pull the latest image, then update the service. The migrations are automatically performed:

```bash
docker-compose pull framasoft/mobilizon
docker-compose up -d mobilizon
```
