# Resource providers

When creating resources, users can chose from predefined resource providers (for instance collaborative text pads, video conferences rooms, etc). The admin can configure which resource providers (and which endpoints) are available for users.

Only the following resource providers softwares are supported for now:

- [Etherpad](https://etherpad.org/)
- [Ethercalc](https://ethercalc.net/)
- [Jitsi Meet](https://meet.jit.si/)

If you want as an admin to add predefined resource providers for your users you can add the following to your configuration:

```elixir
config :mobilizon, Mobilizon.Service.ResourceProviders,
  types: [pad: :etherpad, calc: :ethercalc, visio: :jitsi],
  providers: %{
    etherpad: "https://etherpad.wikimedia.org/p/",
    ethercalc: "https://ethercalc.net/",
    jitsi: "https://meet.jit.si/"
  }
```

!!! tip
    Don't forget the final `/` at the end of the endpoint URL.