# Group page

!!! note
    In Mobilizon version 1.0.0, it is not possible to request access to a group: the **join a group** button is shaded out on the public pages of each group.

    However, we have displayed this button (along with a message explaining that “You can only be invited to groups at the moment”), because we want to integrate such a feature in the future.

    TIn a federated tool, coding this button raises a multitude of complex use cases. We still need time to do things right.

    In the meantime, feel free to comment on a group’s public event to let them know you wish to be invited!

## Report a group

To report a group, you have to:

  1. click ⋅⋅⋅ button
  * click **Report** button:

    ![report group image](../../images/en/report-group.png)

  * [Optional but **recommended**] filling the report modal with a comment:
    ![groupe report modal image](../../images/en/report-group-modal.png)
